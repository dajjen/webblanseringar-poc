<!DOCTYPE html>
<head>
  <link rel="stylesheet" type="text/css" href="stylesheets/normalize.css" />
  <link rel="stylesheet" type="text/css" href="stylesheets/screen.css" />
</head>

<body>
  <nav id="main-menu">
   <img src="images/main-menu.png" />
  </nav>

<div class="grid">
  <figure class="effect-zoe half">
    <img src="images/hallde.png" alt="img22"/>
    <figcaption>
      <h2><a href="release.php"><span>texstar</span>.se</a></h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe half">
    <img src="images/goda_nyheter.png" alt="img22"/>
    <figcaption>
      <h2><a href="release.php"><span>mtg</span>.se</a></h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/insamling.png" alt="img22"/>
    <figcaption>
      <h2><span>svenskinsamlingskontroll</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

<!-- ZOE EFFECT-->

  <figure class="effect-zoe third">
    <img src="images/fora.png" alt="img22"/>
    <figcaption>
      <h2><span>fora</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/bostad.png" alt="img22"/>
    <figcaption>
      <h2><span>bostadsrattsspecialisten</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/goda_nyheter.png" alt="img22"/>
    <figcaption>
      <h2><span>godanyheter</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/hallde.png" alt="img22"/>
    <figcaption>
      <h2><span>hallde</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/holmgrensbil.png" alt="img22"/>
    <figcaption>
      <h2><span>holmgrensbil</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/passivhus.png" alt="img22"/>
    <figcaption>
      <h2><span>passivhuscenter</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>
  <figure class="effect-zoe third">
    <img src="images/fora.png" alt="img22"/>
    <figcaption>
      <h2><span>fora</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/bostad.png" alt="img22"/>
    <figcaption>
      <h2><span>bostadsrattsspecialisten</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/goda_nyheter2.png" alt="img22"/>
    <figcaption>
      <h2><span>godanyheter</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/hallde.png" alt="img22"/>
    <figcaption>
      <h2><span>hallde</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/holmgrensbil.png" alt="img22"/>
    <figcaption>
      <h2><span>holmgrensbil</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/passivhus.png" alt="img22"/>
    <figcaption>
      <h2><span>passivhuscenter</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/fora.png" alt="img22"/>
    <figcaption>
      <h2><span>fora</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/bostad.png" alt="img22"/>
    <figcaption>
      <h2><span>bostadsrattsspecialisten</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/goda_nyheter.png" alt="img22"/>
    <figcaption>
      <h2><span>godanyheter</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/hallde.png" alt="img22"/>
    <figcaption>
      <h2><span>hallde</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/holmgrensbil.png" alt="img22"/>
    <figcaption>
      <h2><span>holmgrensbil</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>

  <figure class="effect-zoe third">
    <img src="images/passivhus.png" alt="img22"/>
    <figcaption>
      <h2><span>passivhuscenter</span>.se</h2>
      <p class="description">Wordpress av Wunderkraut och Nansen</p>
      <p class="icon-links">
        <a href="#"><span class="icon-heart"></span></a>
        <a href="#"><span class="icon-eye"></span></a>
        <a href="#"><span class="icon-paper-clip"></span></a>
      </p>
    </figcaption>
  </figure>
</div>
<!--
<div class="pager">

  <a class="button" href="#till en ny sida som listar alla">Visa alla webblanseringarna</a>

</div>
-->
</body>